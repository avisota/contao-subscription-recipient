<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-subscription-recipient
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_avisota_settings']['avisota_subscription_recipient_cleanup'] = array(
    'Clean recipients',
    'When a recipient remove all its subscriptions, remove him too.'
);

/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_avisota_settings']['subscription_recipient_legend'] = 'Recipients';
