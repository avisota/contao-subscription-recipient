<?php

/**
 * Avisota newsletter and mailing system
 * Copyright © 2016 Sven Baumann
 *
 * PHP version 5
 *
 * @copyright  way.vision 2016
 * @author     Sven Baumann <baumann.sv@gmail.com>
 * @package    avisota/contao-subscription-recipient
 * @license    LGPL-3.0+
 * @filesource
 */


/**
 * Module
 */
$GLOBALS['TL_LANG']['MOD']['avisota-subscription-recipient'] = array(
    'Avisota - Subscription for recipients',
    'Recipient management and subscription for Avisota.'
);

/**
 * Frontend modules
 */
$GLOBALS['TL_LANG']['FMD']['avisota_subscribe']    = array('Recipients - Subscribe');
$GLOBALS['TL_LANG']['FMD']['avisota_activation']   = array('Recipients - Activation');
$GLOBALS['TL_LANG']['FMD']['avisota_unsubscribe']  = array('Recipients - Unsubscribe');
$GLOBALS['TL_LANG']['FMD']['avisota_subscription'] = array('Recipients - Manage subscription');
