<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:15:14+01:00
 */

global $TL_LANG;

$ormAvisotaRecipient = array(
    'activateSubscription'
    => 'Abonnement direkt aktivieren',

    'added_at'
    => 'Hinzugefügt am %s',

    'added_by'
    => ', von <a href="%3$s">%1$s &lt;%2$s&gt;</a>',

    'added_by_unlinked'
    => ', von %1$s &lt;%2$s&gt;',

    'blacklisted'
    => 'Der Abonnent ist geblacklisted',

    'confirm'
    => '%s neue Abonnenten wurden importiert.',

    'confirm-subscription'
    => 'Empfänger <em>%1$s</em> Abonement <em>%2$s</em> ist bestätigt',

    'confirmManualActivation'
    => 'Sind Sie sicher, dass Sie dieses Abonnement manuell aktivieren möchten?',

    'confirm_subscription'
    => 'Abonnement bestätigen',

    'confirmationSent'
    => 'Bestätigungsmail gesendet am %s',

    'doNothink'
    => 'Abonnement unbestätigt eintragen',

    'globally_blacklisted'
    => 'Der Verteiler <strong>%s</strong> befindet sich in der Blacklist. '
       . 'Wenn Sie die Blacklist ignorieren möchten, speichern Sie erneut!',

    'invalid'
    => '%s ungültige Einträge wurden übersprungen.',

    'manually'
    => 'manuell hinzugefügt',

    'personals_legend'
    => 'Persönliche Daten',

    'recipient_legend'
    => 'Empfänger',

    'reminderSent'
    => 'Erinnerungsmail gesendet am %s',
    'remindersSent'

    => '%d. Erinnerungsmail gesendet am %s',

    'remove-subscription'
    => 'Empfänger <em>%1$s</em> Abonement <em>%2$s</em> wurde entfernt',

    'sendConfirmation'
    => 'Bestätigungsmail senden',

    'subscribe'
    => 'Mailingliste abonnieren',

    'subscribe_confirmed'
    => 'Mailingliste abonnieren und aktivieren',

    'subscribe_globally'
    => 'Global abonnieren',

    'subscribe_globally_confirmed'
    => 'Global abonnieren und aktivieren',

    'subscribed'
    => 'registriert am %s',

    'subscription_global'
    => 'Globales Abonnement',

    'subscription_legend'
    => 'Abonnement',

    'subscription_mailingList'
    => 'Mailingliste: %s',

    'unsubscribe'
    => 'Die Mailingliste deabonnieren',

    'unsubscribe_globally'
    => 'Global deabonnieren',

    'addedBy' => array(
        'Hinzugefügt durch',
        'Contao-Benutzer, der diesen Empfänger hinzugefügt hat',
        'von %s',
        'von einem gelöschten Benutzer',
    ),

    'addedById'   => array(
        'Hinzugefügt durch Benutzer ID',
        'Contao Benutzer ID, der diesen Empfänger hinzugefügt hat',
    ),
    'addedByName' => array(
        'Hinzugefügt durch Name',
        'Name des Benutzers, der diesen Empfänger hinzugefügt hat',
    ),

    'addedByUsername' => array(
        'Hinzugefügt durch Benutzername',
        'Contao Benutzername, der diesen Empfänger hinzugefügt hat',
    ),

    'addedOn' => array(
        'Hinzugefügt am',
        'Datum der Einschreibung',
        'Hinzugefügt am %s',
    ),

    'city' => array(
        'Ort',
        'Bitte geben Sie den Namen des Ortes ein.',
    ),

    'company' => array(
        'Firma',
        'Hier können Sie einen Firmennamen eingeben.',
    ),

    'confirmed' => array(
        'Bestätigt',
        'Dieser Account wurde bestätigt.',
    ),

    'copy' => array(
        'Empfänger duplizieren',
        'Duplizieren Sie den Empfänger ID %s.',
    ),

    'country' => array(
        'Land',
        'Bitte wählen Sie ein Land.',
    ),

    'createdAt' => array(
        'Erstellt am',
    ),

    'delete' => array(
        'Empfänger löschen',
        'Löschen Sie den Empfänger ID %s.',
    ),

    'delete_no_blacklist' => array(
        'Empfänger ohne Sperrlisten-Eintrag löschen',
        'Löschen Sie den Empfänger ID %s, ohne ihn der Sperrliste hinzuzufügen.',
    ),

    'edit' => array(
        'Empfänger bearbeiten',
        'Bearbeiten Sie den Empfänger ID %s.',
    ),

    'email' => array(
        'E-Mail',
        'Bitte geben Sie die E-Mail-Adresse ein.',
    ),

    'export' => array(
        'CSV-Export',
        'Exportieren Sie Empfänger in eine CSV-Datei.',
    ),

    'forename' => array(
        'Vorname',
        'Bitte geben Sie den Vornamen des Empfängers an.',
    ),
    'gender'   => array(
        'Geschlecht',
        'Bitte wählen Sie das Geschlecht des Empfängers aus.',
    ),

    'id' => array(
        'ID',
    ),

    'import' => array(
        'CSV-Import',
        'Importieren Sie Empfänger aus einer CSV-Datei.',
    ),
    'lists'  => array(
        'Mailinglisten',
        'Bitte wählen Sie die abonnierte Mailingliste aus.',
    ),

    'mailingListIds' => array(
        'Mailinglist IDs',
    ),

    'mailingListNames' => array(
        'Mailinglist Namen',
    ),

    'mailingLists' => array(
        'Mailinglisten',
    ),

    'migrate' => array(
        'Migrieren',
        'Migrieren Sie Empfänger aus dem Contao-Newslettersystem.',
    ),

    'new' => array(
        'Neuer Empfänger',
        'Fügen Sie einen neuen Empfänger hinzu.',
    ),

    'notify' => array(
        'Empfänger benachrichtigen',
        'Benachrichtigen Sie den Empfänger ID %s.',
    ),

    'postal' => array(
        'Postleitzahl',
        'Bitte geben Sie die Postleitzahl ein.',
    ),

    'remove' => array(
        'CSV-Löschung',
        'Löschen Sie Empfänger auf Basis einer CSV-Datei.',
    ),

    'salutation' => array(
        'Anrede',
        'Bitte wählen Sie die bevorzugte Anrede aus.',
    ),

    'show' => array(
        'Empfängerdetails',
        'Zeigen Sie die Details des Empfängers %s an.',
    ),

    'state' => array(
        'Staat',
        'Bitte geben Sie den Namen des Staates ein.',
    ),

    'street' => array(
        'Straße',
        'Bitte geben Sie den Straßennamen und die Hausnummer ein.',
    ),

    'subscriptionAction' => array(
        'Aktivierung',
        'Bitte wählen Sie die Aktivierungsmethode für Einschreibungen in neue Mailinglisten aus.',
    ),

    'surname' => array(
        'Nachname',
        'Bitte geben Sie den Nachnamen des Empfängers an.',
    ),

    'title' => array(
        'Titel',
        'Bitte geben Sie den Titel des Empfängers an.',
    ),

    'updatedAt' => array(
        'Zuletzt bearbeitet am',
    ),
);

$TL_LANG['orm_avisota_recipient'] = array_merge(
    $TL_LANG['orm_avisota_recipient'],
    $ormAvisotaRecipient
);
