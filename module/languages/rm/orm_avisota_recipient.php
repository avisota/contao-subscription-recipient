<?php
/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:15:14+01:00
 */

global $TL_LANG;

$ormAvisotaRecipient = array(
    'activateSubscription'
    => 'Activar directamain l\'abunament',

    'added_at'
    => 'agiuntà ils %s',

    'added_by'
    => ', da <a href="%3$s">%1$s &lt;%2$s&gt;</a>',

    'added_by_unlinked'
    => ', da %1$s &lt;%2$s&gt;',

    'blacklisted'
    => 'Quest destinatur è sin la black-list',

    'confirm'
    => '%s abunents novs èn vegnids importads.',

    'confirm-subscription'
    => 'L\'abunament dal destinatur <em>%1$s</em> da <em>%2$s</em> è vegnì confermà',

    'confirmManualActivation'
    => 'Es ti segir che ti vuls activar manualmain quest abunament?',

    'confirm_subscription'
    => 'Confermar l\'abunament',

    'confirmationSent'
    => 'Tramess il mail da confermaziun ils %s',

    'doNothink'
    => 'Endatar l\'abunament senza confermaziun',

    'globally_blacklisted'
    => 'Il distributur <strong>%s</strong> sa chatta en la black-list, '
       . 'sche ti vuls ignorar la black-list stos ti memorisar danovamain!',

    'invalid'
    => '%s nunvalaivels èn vegnids surseglids',

    'manually'
    => 'agiuntà manualmain',

    'personals_legend'
    => 'Datas',

    'recipient_legend'
    => 'Destinatur',

    'reminderSent'
    => 'Tramess l\'email da reguranda ils %s',

    'remindersSent'
    => 'Tramess il %d. e-mail da regurdanza ils %s',

    'remove-subscription'
    => 'L\'abunament dal destinatur <em>%1$s</em> da <em>%2$s</em> è vegnì stizzà',

    'sendConfirmation'
    => 'Trametter in e-mail da confermaziun',

    'subscribe'
    => 'Abunar questa glista dad e-mail',

    'subscribe_confirmed'
    => 'Abunar ed activar sin questa glista dad e-mail',

    'subscribe_globally'
    => 'Abunar globalmain',

    'subscribe_globally_confirmed'
    => 'Abunar ed activar globalmain',

    'subscribed'
    => 'registrà ils %s',

    'subscription_global'
    => 'Abunament global',

    'subscription_legend'
    => 'Abunament',

    'subscription_mailingList'
    => 'Glista da mail: %s',

    'unsubscribe'
    => 'De-abunar da questa glista dad e-mail',

    'unsubscribe_globally'
    => 'De-abunar globalmain',

    'addedBy' => array(
        'Agiuntà da ',
        'L\'utilisader da Contau che ha agiuntà quest destinatur.',
        'da %s',
        'dad in utilisader stizzà',
    ),

    'addedById' => array(
        'Agiuntà da l\'utilisader cun l\'ID',
        'L\'ID da l\'utilisader da Contau che ha agiuntà quest destinatur.',
    ),

    'addedByName' => array(
        'Agiuntà da ',
        'Il num da l\'utilisader da Contao che ha agiuntà quest destinatur.',
    ),

    'addedByUsername' => array(
        'Agiuntà da ',
        'Il num d\'utilisader da l\'utilisader da Contao che ha agiuntà quest destinatur.',
    ),

    'addedOn' => array(
        'Agiuntà ils',
        'Data da l\'abunament.',
        'agiuntà ils %s',
    ),

    'city' => array(
        'Lieu',
        'Endatescha il num dal lieu.',
    ),

    'company' => array(
        'Interpresa',
        'Qua pos ti endatar in num d\'ina interpresa.',
    ),

    'confirmed' => array(
        'Confermà',
        'Quest conto è vegnì confermà',
    ),

    'copy' => array(
        'Duplitgar il destinatur',
        'Duplitgar il destinatur cun l\'ID %s',
    ),

    'country' => array(
        'Pajais',
        'Endatscha il num dal pajais.',
    ),

    'createdAt' => array(
        'Creà las',
    ),

    'delete' => array(
        'Stizzar il destinatur',
        'Stizzar il destinatur cun l\'ID %s',
    ),

    'delete_no_blacklist' => array(
        'Stizzar il destinatur senza black-listing',
        'Stizzar il destinatur cun l\'ID %s senza black-listing',
    ),

    'edit' => array(
        'Modifitgar il destinatur',
        'Modifitgar il destinatur cun l\'ID %s',
    ),

    'email' => array(
        'E-Mail',
        'Endatescha l\'adressa dad e-mail.',
    ),

    'export' => array(
        'Export da CSV',
        'Exportar ils destinaturs ad ina datoteca da CSV',
    ),

    'forename' => array(
        'Prenum',
        'Tscherna il prenum dal destinatur.',
    ),

    'gender' => array(
        'Schlattaina',
        'Tscherna la schlattaina dal destinatur.',
    ),

    'id' => array(
        'ID',
    ),

    'import' => array(
        'Import da CSV',
        'Importar ils destinaturs dad ina datoteca da CSV.',
    ),

    'lists' => array(
        'Glistas da mail',
        'Tscherna las glistas da mail abunadas.',
    ),

    'mailingListIds' => array(
        'IDs da las glistas da mail',
    ),

    'mailingListNames' => array(
        'Nums da las glistas da mail',
    ),

    'mailingLists' => array(
        'Glistas da mail',
    ),

    'migrate' => array(
        'Migrar',
        'Migrar destinaturs dal sistem da newsletter da Contao.',
    ),

    'new' => array(
        'Nov destinatur',
        'Agiuntar in nov destinatur',
    ),

    'notify' => array(
        'Avisar il destinatur',
        'Avisar il destinatur cun l\'ID %s',
    ),

    'postal' => array(
        'Number postal',
        'Endatscha il numer postal.',
    ),

    'remove' => array(
        'Stizzar da CSV',
        'Stizzar ils destinaturs dad ina datoteca da CSV.',
    ),

    'salutation' => array(
        'Salid',
        'Tscherna il salid preferì.',
    ),

    'show' => array(
        'Detagls dal destinatur',
        'Mussar ils detagls dal destinatur cun l\'ID %s',
    ),

    'state' => array(
        'Stadi',
        'Endatscha il num dal stadi',
    ),

    'street' => array(
        'Via',
        'Endatscha il num ed il numer da la via',
    ),

    'subscriptionAction' => array(
        'Activar',
        'Tscherna ina metoda per activar novs abunaments sin glistas da mail.',
    ),

    'surname' => array(
        'Num',
        'Tscherna il num dal destinatur',
    ),

    'title' => array(
        'Titel',
        'Tscherna il titel dal destinatur',
    ),

    'updatedAt' => array(
        'Ultima modificaziun las',
    ),
);

$TL_LANG['orm_avisota_recipient'] = array_merge(
    $TL_LANG['orm_avisota_recipient'],
    $ormAvisotaRecipient
);
