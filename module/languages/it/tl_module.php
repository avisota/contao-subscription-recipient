<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/it/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-04-07T04:01:36+02:00
 */

global $TL_LANG;

$tlModule = array(
    'avisota_recipient_fields' => array(
        'Dati personali',
    ),
);

$TL_LANG['tl_module'] = array_merge(
    $TL_LANG['tl_module'],
    $tlModule
);
